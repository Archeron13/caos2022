#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>

// Implementation of LS 

static char perms_buff[30]; 

const char *get_perms(mode_t mode) // Used to obtain information about file permission
{
  char ftype = '?';

  if (S_ISREG(mode)) ftype = '-';
  if (S_ISLNK(mode)) ftype = 'l';
  if (S_ISDIR(mode)) ftype = 'd';
  if (S_ISBLK(mode)) ftype = 'b';
  if (S_ISCHR(mode)) ftype = 'c';
  if (S_ISFIFO(mode)) ftype = '|';

  sprintf(perms_buff, "%c%c%c%c%c%c%c%c%c%c %c%c%c ", ftype,
  mode & S_IRUSR ? 'r' : '-',
  mode & S_IWUSR ? 'w' : '-',
  mode & S_IXUSR ? 'x' : '-',
  mode & S_IRGRP ? 'r' : '-',
  mode & S_IWGRP ? 'w' : '-',
  mode & S_IXGRP ? 'x' : '-',
  mode & S_IROTH ? 'r' : '-',
  mode & S_IWOTH ? 'w' : '-',
  mode & S_IXOTH ? 'x' : '-',
  mode & S_ISUID ? 'U' : '-',
  mode & S_ISGID ? 'G' : '-',
  mode & S_ISVTX ? 'S' : '-');

  return (const char *)perms_buff;
}

void ls(const char *dir,int argl)
{
    char datestring[256];
    struct passwd pwent;
    struct passwd *pwentp;
    struct group grp;
    struct group *grpt;
    struct dirent *d;
    DIR *location = opendir(dir);
    struct stat fstat;

    char holder[512];
    struct tm time;

    if (!location)
    {
        exit(EXIT_FAILURE); // In case of wrong directory
    }
    while ( ( d = readdir(location)) != NULL ) 
    {
        if (d->d_name[0] == '.')
        {
                continue;
        }
        if (!argl)
        {
            printf("%s  ", d->d_name);
        }
        else{
            sprintf(holder, "%s/%s", ".", d->d_name); // Appending the directory as d->d_name is not absolute
            stat(holder, &fstat);
            printf("%10.10s ", get_perms(fstat.st_mode)); // print info about file permission
            getpwuid_r(fstat.st_uid, &pwent, holder, sizeof(holder), &pwentp); // Gives the name assosciated to UID
            printf("%s ", pwent.pw_name); // Prints the name assosciated to UID 
            getgrgid_r (fstat.st_gid, &grp, holder, sizeof(holder), &grpt); // Same as above but for GID
            printf("%s ",grp.gr_name); // Prints name assosciated with GID

            printf("%ld ", fstat.st_nlink); // Prints number of hard links
            printf("%zu ",fstat.st_size);  // Prints total size 
            localtime_r(&fstat.st_mtime, &time);
            strftime(datestring, sizeof(datestring), "%F %T", &time);  // Prints time of last modification

            printf(" %s %s\n", datestring, d->d_name); // Prints name of file

        }
    }
    closedir(location);

}

int main(int argc, const char *argv[])
{
    int argl=0;
    if (argc == 1) // In case no arguments are given
    {
        ls(".",argl);
    }
    else if (argc == 2) // The case where argument is given
    {                   // If the argument is -l then directory is assumed 
                        // to be present working directory
        if (argv[1][0] == '-')
        {
            char *option  = (char *)(argv[1] + 1);
            if (*option == 'l')
            {
                argl = 1;
            }
            ls(".",argl);
        }
        else
        {
            char *dir = argv[1];
            ls(dir,argl);   
        }
    }
    else if (argc == 3) // Assumed that the directory is present
    {                   // Before the l flag
        char *dir = argv[1];
         if (argv[2][0] == '-')
        {
            char *option  = (char *)(argv[2] + 1);
            if (*option == 'l')
            {
                argl = 1;
            }
            ls(dir,argl);
        }

    }
    else
    {
        perror("Invalid Usage");
    }

}