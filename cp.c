#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include<unistd.h>


int copy_file(const char* src_path, const char* dest_path) 
{
    FILE* src_file = fopen(src_path, "r");
    if (src_file == NULL) 
    {
        perror("Failed to open source file");
        return 1;
    }

    // Open the destination file
    FILE* dest_file = fopen(dest_path, "w");
    if (dest_file == NULL) 
    {
        perror("Failed to open destination file");
        fclose(src_file);
        return 1;
    }

    // Copy the contents of the source file to the destination file
    char buffer[1024];
    size_t bytes_read;
    while ((bytes_read = fread(buffer, 1, sizeof(buffer), src_file)) > 0) 
    {
        fwrite(buffer, 1, bytes_read, dest_file);
    }

    // Close the files
    fclose(src_file);
    fclose(dest_file);

    return 0;
}

int file_directory(const char *src, const char *dst) 
{
      // Get the filename from the source path
    char *filename = strrchr(src, '/');
    if (filename == NULL) 
    {
        filename = (char *)src;
    } else 
    {
        filename++;
    }

    // Create the destination path
    char dst_path[4096];
    snprintf(dst_path, sizeof(dst_path), "%s/%s", dst, filename);

    // Open source and destination files
    int src_fd = open(src, O_RDONLY);
    int dst_fd = open(dst_path, O_WRONLY | O_CREAT, 0644);

    // Copy file contents
    char buffer[4096];
    ssize_t bytes_read;
    while ((bytes_read = read(src_fd, buffer, sizeof(buffer))) > 0) 
    {
        write(dst_fd, buffer, bytes_read);
    }

    // Close files
    close(src_fd);
    close(dst_fd);
    return 0;
}


int copy_directory(const char* src_path, const char* dest_path) 
{
    // Open the source directory
    DIR* src_dir = opendir(src_path);
    if (src_dir == NULL) 
    {
        perror("Failed to open source directory");
        return 1;
    }

    // Create the destination directory if it doesnt exists
    mkdir(dest_path, 0755);
    

    // Iterate through the contents of the source directory
    struct dirent* entry;
    while ((entry = readdir(src_dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) 
        {
            continue;
        }

        // Construct the full path of the current entry
        char src_entry_path[4096];
        snprintf(src_entry_path, sizeof(src_entry_path), "%s/%s", src_path, entry->d_name);

        char dest_entry_path[4096];
        snprintf(dest_entry_path, sizeof(dest_entry_path), "%s/%s",dest_path, entry->d_name);

        // Check if the current entry is a directory
        if (entry->d_type == DT_DIR) 
        {
            // Recursively copy the directory
            if (copy_directory(src_entry_path, dest_entry_path) != 0) {
                closedir(src_dir);
                return 1;
            }
        } else 
        {
            // Copy the file
            if (copy_file(src_entry_path, dest_entry_path) != 0) {
                closedir(src_dir);
                return 1;
            }
        }
    }
    closedir(src_dir);

    return 0;
}

int main(int argc, char* argv[]) 
{
    if (argc != 3) 
    {
        printf("Use like this: %s <source> <destination>\n", argv[0]);
        return 1;
    }
    const char* src_path = argv[1];
    const char* dest_path = argv[2];

    // Check if the source path is a directory
    DIR* src_dir = opendir(src_path);
    DIR* dest_dir = opendir(dest_path);
    if (src_dir != NULL) 
    {
        closedir(src_dir);
       // If the source path is a directory, recursively copy it
      return copy_directory(src_path, dest_path);
    } 
    else if (dest_dir != NULL) 
    {
        closedir(dest_dir);
        file_directory(src_path,dest_path);
    }
    else 
    {
     // If the source path is a file, copy it
     return copy_file(src_path, dest_path);
    }

}

