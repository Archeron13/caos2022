#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define N 3
#define M 2
#define K 2


// Hold the column and indices of elements
struct holder 
{
    size_t i;
    size_t j;
};

double A[N][M] = 
{
    {1.0, 2.0},
    {1.0, 2.0},
    {2.0, 123123.0}
};
double B[M][K] = 
{
    {2.0, 3.0},
    {4.0, 5.0}
};

double C[N][K];

void *multiply(void *arg) // If I pass as struct holder I get error so 
                                    // I am passing as void and then reconverting
{
    struct holder *h = (struct holder *)arg;
    size_t i = h->i;
    size_t j = h->j;
    double sum = 0;
    int l;
    for (l = 0; l < M; l++) 
    {
        sum += A[i][l] * B[l][j];
    }
    C[i][j] = sum;
    free(arg);
    return NULL;

}
int main() 
{
    pthread_t threads[N*K];
    int thread_count = 0;
    size_t i, j;
    for (i = 0; i < N; i++) 
    {
        for (j = 0; j < K; j++) 
        {
            struct holder *h = (struct holder *) malloc (2*sizeof(struct holder ));
            h->i=i;
            h->j=j;
            pthread_create(&threads[thread_count++], NULL, &multiply, h); // I am not sure but
                                                                        // Using i+j instead of Threadcount is giving error
                                                                        // So i am keeping threadcount despite it looking a bit
                                                                        // Redundant
            // // Creating a thread to calculate element

        }
    }
    // Waiting for the threads to finish
    for (i = 0; i < N*K; i++) 
    {
       pthread_join(threads[i], NULL);
    }

    //Print the answer

    for (i = 0; i < N; i++) 
    {
        for (j = 0; j < K; j++) 
        {
            printf("%f\t", C[i][j]);
        }
        printf("\n");
    }
    return 0;

}
