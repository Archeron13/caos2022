#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct elem {
    int32_t value;
    uint32_t next;
};

int main(){

   FILE *ptr;

   ptr = fopen("binary.bin","rb");
   if(ptr == NULL){
       printf("ERROR: Invalid pointer to file ");
       exit(1);
   }
   struct elem num = {.next=100, .value=200};
   while (num.next != 0){
       fread(&num,sizeof(struct elem),1,ptr);
       printf("%i\t",num.value);
       printf("%i\n",num.next);
       if (ptr == NULL){
            printf("ERROR: Invalid pointer to file ");
            exit(1);
        }
        else if (ptr == EOF){
            printf("ERROR: EOF too early");
            exit(1);
        }

   }
   fclose(ptr);
   return 0;

}

