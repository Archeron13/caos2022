Float-Parser

IMPORTANT: Please compile with a -lm flag for log2 to not be an undefined reference

A C program to display the bitwise representation of floats.
Macros were used for special cases such as +INFINITE, NaN etc.
It was not possible for me to find a way to reliably check the difference between sNaN and qNaN so for now it is on hold.

Float-Parser first takes a float and then passes it to a function bitwise_representation which returns a pointer to a static array of 32 bits.
In bitwise_representation, first, the argument is checked for special cases such as +INFINITY,NaN,+0,-0 etc.
Then the integer part is taken from the argument and converted to binary and the bits are inputted into the static array.
Then, the biased exponent is calculated and converted to binary and the exponent bits are inputted into the static array.
In the final step, the fractional part is taken from the argument by removing the integer part of the argument and then converted to binary before finally being inputting into the static array.
Finally, the static array is returned.

A for loop is created for the sake of printing elements of the static array which are our bitwise representations.
