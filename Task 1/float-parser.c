// PLEASE COMPILE WITH -lm flag 
#include <stdio.h>
#include <math.h>


int *bitwise_representation(float num){
    static int arr[32] = {0};
    if (num == 0){
        if (signbit(num)){
            arr[0]=1;
        }
        return arr;
    }
    else if ( (isnanf) (num)){
        for (int i=1;i<32;++i){
            arr[i]=1;
        }
        return arr;
    }
    // Need to do a different version as it is not supporting qNaN
    else if ( (__issignalingf) (num) ){ 
         for (int i=0;i<32;++i){
            arr[i]=1;
        }
        return arr;
    }

    else if (num == INFINITY){
        for (int i=1;i<9;++i){
            arr[i]=1;
        }
        return arr;
    }

    else if ( num < -__FLT_MAX__){
        for (int i=0;i<9;++i){
            arr[i]=1;
        }
        return arr;
    }
    else if (num >= 0){
        arr[0]=0;
    }
    else{
        arr[0]=1;
    }
    // Taking the integer part of num
    int itg_part = num; 
    // Taking the Fractional Part of num
    float frac_part = num - itg_part; 
    // Iterator for storing values in our arr
    int iterator= 0 ;
    // To calculate number of bits of integer
    int no_of_bits = log2(itg_part) + 1; 
    // PLEASE COMPILE WITH .lm flag for log2 to not be an undefined reference
    int temp_arr[no_of_bits];
    while (itg_part>0){
        temp_arr[iterator]=itg_part%2;
        iterator += 1;
        itg_part = itg_part/2;
    }
    iterator = 8;
    for(int i = no_of_bits-1;i>=0;--i){
        arr[iterator] = temp_arr[i];
        iterator += 1; 
    }
    int biased_exponent = 127 + iterator - 9;
    // To calculare number of bits of biasesd exponent
    no_of_bits = log2(biased_exponent) + 1; 
    // To hold the bits for exponent temporarily
    int temp_arr1[no_of_bits]; 
    // Converting Exponents to bits and inputting in our arr
    int temp_iterator = 0;
    while (biased_exponent>0){
        temp_arr1[temp_iterator]=biased_exponent%2;
        temp_iterator += 1;
        biased_exponent=biased_exponent/2;
    }
    temp_iterator = 1;
    for (int i=no_of_bits-1;i>=0;--i){
        arr[temp_iterator] = temp_arr1[i];
        temp_iterator += 1;
    }
    
    while (iterator < 32){
        if (frac_part == 0){
            break;
        }
        else{
            frac_part *= 2;
            //Taking integer part of frac again
            itg_part = frac_part; 
            // inputting integer part
            arr[iterator] = itg_part; 
            // Removing the integer part
            frac_part = frac_part - itg_part; 
        }
        iterator +=1;

    }



    return arr;

}

int main(){
    float num;
    scanf("%f",&num);
    int *ptr = 0;
    ptr = bitwise_representation(num);
    for(int i=0;i<32;++i){
        printf("%d",ptr[i]);
    }
}